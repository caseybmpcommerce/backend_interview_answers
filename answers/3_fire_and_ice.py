'''
Create an array of the gender for the first 100 characters returned from the /characters API from:
https://anapioficeandfire.com/Documentation
'''

import json

import requests


def json_decode(string):
    return json.loads(string)


genders = []
for page in [1, 2]:
    url = f'https://www.anapioficeandfire.com/api/characters?page={page}&pageSize=50'
    response = requests.get(url)
    characters = json_decode(response.text)
    genders += [character['gender'] for character in characters]

print(genders)
