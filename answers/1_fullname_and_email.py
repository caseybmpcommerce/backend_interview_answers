'''

SOURCE: https://edabit.com/challenge/gB7nt6WzZy8TymCah

Create the instance attributes fullname and email in the Employee class. Given a person's first and last names:

Form the fullname by simply joining the first and last name together, separated by a space.
Form the email by joining the first and last name together with a . in between, and follow it with @company.com at the end. Make sure the entire email is in lowercase.
'''


class Employee:
    def __init__(self, first_name, last_name, bonus, salary):
        self.firstname = first_name
        self.lastname = last_name
        self.bonus = bonus
        self.salary = salary
        self.email = '{}.{}@company.com'.format(first_name, last_name).lower()


emp_1 = Employee(first_name="John", last_name="Smith", bonus=0, salary=50000)
emp_2 = Employee(first_name="Mary", last_name="Sue", bonus=10000, salary=50000)
emp_3 = Employee(first_name="John", last_name="Smith", bonus=10000, salary=90000)

'''
emp_1.fullname ➞ "John Smith"
emp_2.email ➞ "mary.sue@company.com"
emp_3.bonus ➞ 10000
emp_3.bonus_and_salary -> 100000
'''
