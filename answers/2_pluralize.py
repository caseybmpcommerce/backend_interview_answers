'''
Given a list of words in the singular form, return a set of those words in the plural form if they appear more than once
in the list.
'''


def pluralize(lst):
    x = set()
    for item in lst:
        if lst.count(item) > 1:
            x.add(item + "s")
        else:
            x.add(item)
    return x


pluralize(["cow", "pig", "cow", "cow"])
pluralize(["table", "table", "table"])
pluralize(["chair", "pencil", "arm"])

'''
pluralize(["cow", "pig", "cow", "cow"]) ➞ { "cows", "pig" }
pluralize(["table", "table", "table"]) ➞ { "tables" }
pluralize(["chair", "pencil", "arm"]) ➞ { "chair", "pencil", "arm" }
'''
